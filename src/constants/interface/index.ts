export interface KoaResultInterface {
    message: string,
    status: number,
    data: any
}

export class KoaResult implements KoaResultInterface {
    data: any;
    message: string;
    status: number;

    constructor(data: any, message: string, status: number) {
        this.data = data;
        this.message = message;
        this.status = status
    }
}

export interface RegisterUserParams {
    username: string,
    password: string,
    mobile: string
    sex: string,
}