import {KoaResult} from "../constants/interface/index";

export default   async (ctx: any, next: any) => {
    ctx.type = 'application/json';
    ctx.json = function (data: any, message: string, status: number) {
        ctx.body = new KoaResult(data, message, status);
    }
    ctx.failJson = function (data: any, message: string, status: number = 40004) {
        ctx.body = new KoaResult(data, message, status);
    }
    ctx.successJson = function (data: any, message: string, status: number = 0) {
        ctx.body = new KoaResult(data, message, status);
    }
    await next();
}