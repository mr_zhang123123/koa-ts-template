import {encrypByMD5} from "../utils/index";
const db = require("../app/database");


interface UserModelInterface {
    name: string;
    id: number,
    username: string,
    password: string,
    mobile: string,
    sex: string,
    create_time: string,
    modify_time: string,
    is_delete: string,
    avatar_url:string
}

class UserModel {
    public static async create(params:any) {
        const encrypPassword:string = encrypByMD5(params.password);
        const statement = `insert into tb_user(username,password,sex,mobile) values('${params.username}','${encrypPassword}','${params.sex}','${params.mobile}')`
        const [userRes,] = await db.execute(statement);
        return [userRes,];
    }

    public static remove(){

    }

    public static update(){
        
    }

    public static async query(mobile:string){
        const [res,] = await db.execute(`select * from tb_user where mobile = ${mobile}`);
        return [res,];
    }
}

export {
    UserModelInterface as Interface,
    UserModel
}