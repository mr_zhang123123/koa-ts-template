const userService = require("../service/userService");

class UserController {
    public async login(ctx: any, next: Function) {
        ctx.json({ name: "login" }, "我是数据", 0);
    }

    public async register(ctx: any, next: Function) {
        const body: object = ctx.request.body;
        await userService.register(ctx, next, body);
    }

    public async getUserInfoById(ctx: any, next: Function) {
        const id: string = ctx.params;
        await userService.getUserInfoById(ctx, next, { id });
    }
}

module.exports = new UserController();
