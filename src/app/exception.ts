import {
    NAME_OR_PASSWORD_IS_REQUIRE,
    USER_ALREADY_EXITS,
    USER_NO_EXITS,
    USER_NAME_PASSWORD_WRONG,
    UNAUTHORIZATION,
    UNPERMISSION
} from "../constants/errorType"

import {KoaResultInterface} from "../constants/interface/index";

const errorHandler = async (error: Error, ctx: any) => {
    let status, message;
    switch (error.message) {
        case NAME_OR_PASSWORD_IS_REQUIRE:
            status = 400
            message = '用户名或者密码不能为空'
            break
        case USER_ALREADY_EXITS:
            status = 409
            message = '该用户手机号已注册'
            break
        case USER_NO_EXITS:            
            status = 400
            message = '用户名不存在,请重新注册'
            break
        case USER_NAME_PASSWORD_WRONG:
            status = 400
            message = '用户名或密码错误'
            break
        case UNAUTHORIZATION:
            status = 401
            message = '授权无效'
            break
        case UNPERMISSION:
            status = 401
            message = '无权限操作'
            break
        default:
            status = 404
            message = '未知错误';
    }
    
    let res: KoaResultInterface = {
        message,
        status,
        data: null
    }
    ctx.body = res;
    console.log(ctx.body);
}

module.exports = errorHandler
