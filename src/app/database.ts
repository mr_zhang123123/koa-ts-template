const mysql = require('mysql2')
const config = require('./config')

const connections = mysql.createPool({
    host: config.db.host,
    port: config.db.port,
    database: config.db.database,
    user: config.db.user,
    password: config.db.password
})

connections.getConnection((err: any, conn: any) => {
    if(err){
        console.log(err);
        return;
    }
    conn.connect((err: any) => {
        if (err) {
            console.log('mysql connect err', err)
        } else {
            console.log('mysql connect success')
        }
    })
}) 

module.exports = connections.promise()
