import "./globalData";
import "./test"

const config = require("./app/config");
const Koa: any = require("koa");
const cors: Function = require("koa2-cors");
const serve: Function = require("koa-static");
const app: any = new Koa();
const router = require("./router");
// const bodyParser = require('koa-bodyparser');
const bodyParser = require('koa-body');
const errorHandler = require('./app/exception');
const { jsonMiddle } = require("./middleware");
const db = require("./app/database");


app.use(serve(__dirname + "/public")); // 部署静态资源
app.use(jsonMiddle); // ctx.json 方法支持
app.use(bodyParser({
    formidable: { uploadDir: __dirname + "/public/upload" },
    multipart: true,
    urlencoded: true
})) // 请求体获取
app.use(cors()); // 允许跨域
app.use(router.routes()) // 挂在路由

app.listen(config.port, () => {
    console.log("APP IS STARTED ON PORT " + config.port)
})

app.on('error',errorHandler) //监听错误信息


export default {}
