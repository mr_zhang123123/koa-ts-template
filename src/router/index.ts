const Router = require("koa-router");
const fs = require("fs");
const {sep} = require('path')
const router = new Router();

router.prefix("/api");

const isDirectory = (filePath: string): boolean => {
    const fileStat = fs.statSync(filePath);
    return fileStat.isDirectory()
}

const eachFile = (fileName: string): any => {
    const isDir: boolean = isDirectory(fileName);
    if (isDir) {
        fs.readdir(fileName, (err: any, res: any) => {
            if (res) {
                res.forEach((item: string) => {
                    eachFile(fileName + `${sep}` + item);
                })
                return
            }
        })
    } else {
        const route = require(fileName)
        router.use(route.routes());
    }
}

/**
 * 遍历router下所有文件，并基于commJs导入router
 */
eachFile(__dirname);

module.exports = router
