const Router = require("koa-router");
const userRouter = new Router();
const userController = require("../controller/userController");


userRouter.prefix("/user");


userRouter.get("/info/:id", userController.getUserInfoById)
userRouter.post("/login", userController.login)
userRouter.post("/register", userController.register)

module.exports = userRouter
export {}